# Examples

Please refer to the main [READE](../README.md) on how to start the dedicated build-pipeline.
I recommend using [latexmk](https://mg.readthedocs.io/latexmk.html).

You can copy the content below to your bashrc or zshrc

```bash
TEX_FILTER="natbib|usr/share|on input|.tex|line [0-9]*|bibitem|Overfull|Underfull|^#|^$|pgfplots Warning|[0-9]+\.|[a-z]*)|.*File.*empty|Have you used any entries.*|by a fixed one|No file [a-z]+\.[a-z]+[\.]|chapter without number|to be read again|\[\]|.*\\relax|duplicate ignored|\\end{frame}|\\endgroup|\\pgfpages|\\frame"

function latexmkview(){
   latexmk -e '$preview_continuous_mode="1"' -e '$pdf_previewer="start okular"' -cd -f ${1%.tex} | grep -v -E "${TEX_FILTER}";
}

function latexmkpresent(){
   latexmk -e '$preview_continuous_mode="1"' -e '$pdf_previewer="start pympress"' -cd -f ${1%.tex} | grep -v -E "${TEX_FILTER}";
}
```

which runs latexmk and then opens either [Okular](https://okular.kde.org/de/) or [pympress](https://github.com/Cimbali/pympress/)
after compilation.
This will also update the file automatically on-the-fly, if a file is modified.

```bash
latexmkview article_example.tex
latexmkpresent beamer_example.tex
latexmkview beamer_example.tex
latexmkview thesis_example.tex
```