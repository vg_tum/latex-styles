
A detailed list of the used acronyms, notation, symbols, indices and operators 
can be found at the very end of this thesis -- starting from page
\pageref{ch:glossary}.
Individual symbols and functions are introduced in the remainder of this thesis
and are unique.
An exception is given by hyper-parameters \gls{HyperNot},
thresholds \gls{ThresholdNot}, 
as well as upper- and lower-bound limits \gls{LowerLimit} and \gls{UpperLimit},
which are solely defined by their indices.
Similarly, indexing variables \gls{IndexVariables},
and \gls{SizeVariables} are only used to denote iterations
or specific values of other containers.

% Matrix / Vector
In order to outline the notation or generic relations, 
we use an arbitrarily chosen placeholder variable \gls{Dummy}.
Given this placeholder variable as a scalar term,
we denote vectors as $\gls{VecNot} \in\RE[n]$ and matrices as $\gls{MatNot}\in\RE[m][n]$
.\footnote{This convention does not hold for typed letters}
Explicit elements of matrices or vectors are denoted as \gls{MatElemNot},
while the transpose is denoted as \gls{TransposeNot}.
We denote norms as~\gls{NormNot}, \ie/, $\norm{2}{\gls{VecNot}}$
represents the euclidean norm.
The identity vector and identity matrix are denoted as \gls{OneVecNot} and \gls{OneMatNot},
and similarly as \gls{ZeroVecNot} and \gls{ZeroMatNot} as the zero vector and zero matrix.


% time & iterations
A temporal sequence of vectors \gls{VecNot} over time is described as a 
trajectory $\gls{TrajNot}\EqDef\tuple{\sseqcom{\gls{VecNot}}{\tm}{1}{2}{T}}$,
using the time-index convention \gls{TimeIdxNot}, where \gls{time} 
represents the time.
The first-order time derivative is denoted as \gls{DotNot}.
In order to increase readability, the time indexing may be omitted and every 
variable is expected to be denoted as $\gls{Dummy}\Tt$.
For these cases, the temporal successor and predecessor 
are emphasized as $\gls{SuccessorNot} \EqDef \gls{Dummy}\Tp$ and 
$\gls{PredecessorNot} \EqDef \gls{Dummy}\Tm$,
where $\gls{AssignmentNot}$ expresses equal by definition
in the scope of this thesis.
Similarly, the posterior as \eg/ in Bayesian
inference is denoted as \gls{PosteriorNot}.
In case an algorithm is run in a cyclic manner, 
the current iteration is indexed as \gls{IteraIdxNot}.

% indexing
If we refer to a member of containers such as sets, lists, 
vector, \etc/ we denote \gls{ContentIterNot} 
as a specific scalar value of the former.
In contrast to vectors, lists are only used within algorithms,
while we denote sets as \gls{SetVarNot}.
For sets, we further denote 
the union as \gls{UnionNot},
the intersection as \gls{InterNot}, 
the set-equality as \gls{SetEqualityNot},
the difference as \gls{SetDifferenceNot} and 
the empty set as \gls{EmptySetNot}.
In order to represent the size of vectors, we use \gls{CardinalityNot}. 
If \gls{CardinalityNot} is applied on sets, the cardinality is used.
Eventually, we denote hierarchical systems by denoting layer $k$ as \gls{HierLayNot}.

% MULTI-Agent
In the context of multi-agent settings, $\gls{Dummy}$ is the scalar variant
that is not assigned to any specific agent.
In contrast to that, $\gls{AgIdxNot}$ represents a variable explicitly assigned to agent $i$,
while
$\agstuple{\gls{Dummy}}$ denotes the \textit{joint} team-analogue of said variable over all agents.
For the sake of brevity, $\agstuple{\gls{Dummy}}$ is most commonly denoted as
$\gls{AgAllNot}$.
Similarly, $\gls{AgOtherIdxNot}$ wraps all elements of $\agstuple{\gls{Dummy}}$ except ${\gls{Dummy}}\Ai$.
Within dyadic~\ac{HRC}, ${\gls{Dummy}}\Ahum$ expresses
the variable being assigned to the human and ${\gls{Dummy}}\Arob$ to the robot.
State-spaces are denoted in calligraphic letters, 
\eg/,~the action-space $\ActionSpace \EqDef \set{\sActionSpace\Ahum , \sActionSpace\Arob}$,
with the exception of \gls{GaussNot} expressing a Gaussian distribution over \gls{Dummy}.
In case multiple variables need to be indexed identically, we denote this 
by wrapping the dedicated variables in tuples, \eg/, we simplify
$$
\begin{aligned}
	%  \tuple{a,b,c}\Itk &\EqDef a\Itk, b\Itk, c\Itk \\
	\tuple{a,b,c}\oA &\EqDef a\oA, b\oA, c\oA
\end{aligned}\,.
$$

% stochastic
In the context of stochastic variables, we denote~\acp{PDF} as $\probaof{\dummyVar}$ and
conditionally dependent~\acp{PDF} as $\probacond{\dummyVar\idx{1}}{\dummyVar\idx{2}}$.
Similarly, 
$\expectof[\dummyVar\idx{1}\drawn\distribover{\dummyVar\idx{2}}]{}$ and 
$\varof[\dummyVar\idx{1}\drawn\distribover{\dummyVar\idx{2}}]{}$ 
symbolize the expectation and variance of random variable $\dummyVar\idx{1}$.
This variable $\dummyVar\idx{1}$ follows a~\ac{PDF} $\distribover{\cdot}$, 
which depends on $\dummyVar\idx{2}$; and where \gls{BlankNot} represents 
a blank input.


% optimization etc
If \gls{Dummy} is used to optimize an objective, 
the optimal solution is denoted as $\gls{OptNot}$.
Within regression or empirical evaluations, for which the actual
ground-truth is known, we denote the ground-truth as \gls{GroundTruthNot}.
In case either the true optimum or ground-truth is estimated
from collected experience, \ie/,~evidence, we denote
the estimate as \gls{EstimateNot}, 
the currently best performing sample as \gls{BestDataNot} 
and the worst performing observed data sample as \gls{WorstDataNot}.
If a function is approximated by means of~\aclp{NN}, we use
\gls{TargetNetNot} to denote a \emph{target network}.

% 
For kinematic robot chains, we refer to the origin of the chain as base $\baseFr$,
the \ee/ as $\eeFr$, and the tool as $\toolFr$.
Coordinate transformation matrices are denoted as 
$\Transform{{\color{vg:reference:frame}\mathfrak{y}}}{{\color{vg:reference:frame}\mathfrak{z}}}$
and rotation matrices as 
$\Rotate{{\color{vg:reference:frame}\mathfrak{y}}}{{\color{vg:reference:frame}\mathfrak{z}}}$,
as a transformation / rotation 
from ${\color{vg:reference:frame}\mathfrak{y}}$ to ${\color{vg:reference:frame}\mathfrak{z}}$.
We denote \gls{RRoll},\gls{RPitch} and \gls{RYaw} as the rotation matrices
% We refer to $\Rotate{\gls{Dummy}}{}$ as a rotation around \gls{Dummy},
% where \eg/,~$\gls{roll}, \gls{pirch}, \gls{yaw}$ denote rotations 
around 
coordinate axes \gls{rviz_x}, \gls{rviz_y} and \gls{rviz_z}.
Regarding translational notations,
$\sca[\baseFr]{\gls{VecNot}}[\eeFr\toolFr]$ describes a vector
$\gls{VecNot}$ pointing from the \ee/ to the tool, expressed in the base-frame.
If no explicit reference frame is provided, the variable is given \wrt/ $\baseFr$
for robotic systems and \wrt/ the world-frame for generic settings.

% Boolean and set operations
Eventually, for Boolean values we denote \emph{True} and \emph{False}
as \gls{TrueSymbol} and \gls{FalseSymbol},
and denote a logical \emph{AND} as \gls{ANDNot}
and a logical \emph{OR} as \gls{ORNot}.