# LATEX style files

All custom classes and files are to be found in the
[texmf](./texmf/texmf/) directory, cf. [TEXMF root directories](https://miktex.org/kb/texmf-roots).

## Usage / Installation

Please follow the steps below to use the presented LaTeX-styles as needed.

> **NOTE: This repo assumes you have already installed and setup LaTeX**

In order to use the style-files / custom packages, this GIT uses the
[TEXMF root directory](https://miktex.org/kb/texmf-roots).

This can either be set globally or specifically for a dedicated project.

### Globally

Open a terminal and check your current Texmfhome

```bash
kpsewhich -var-value=TEXMFHOME
--> e.g. /home/gabler/texmf
```

For a global usage, simply copy the texmf directory in said directory, e.g.

> **WARNING: This command will overwrite existing packages in your current setup, so do not blindly copy this command**

```bash
git@gitlab.com:vg_tum/latex-styles.git
cd latex-styles
rsync -avPh texmf/ $(kpsewhich -var-value=TEXMFHOME)
```

### Temporary

A temporary texmf directory is also possible but strongly relies on your build procedure.

#### Simple and dirty

Copy all packages in your root directory, you should now be able to include the packages as presented in the [example-directory](./examples/).



#### latexmk

> **NOTE:** as Overleaf also relies on latexmk internally (cf [https://www.overleaf.com/learn/latex/Articles/How_to_use_latexmkrc_with_Overleaf](https://www.overleaf.com/learn/latex/Articles/How_to_use_latexmkrc_with_Overleaf)), this procedure is also applicable on [https://www.overleaf.com/](https://www.overleaf.com/)

For [latexmk](https://mg.readthedocs.io/latexmk.html), you simply have to add a ```latexmkrc``` file.
In this latexmkrc, you can set the TEXMF-path to a current local path to your liking.
You may want to use a relative path as shown in the [latexmkrc in the example-directory](./examples/latexmkrc):

```bash
$ENV{'TEXINPUTS'}='./..texmf//:' . $ENV{'TEXINPUTS'}; 
```

This allows then to simply use latexmk syntax in that directory without copying content.

#### Texstudio and Texmaker

> **NOTE: For Windows PCs you may have to run some further options, which I am not aware of. So I would suggest to use the global version instead.**

As neither Texstudio nor Texmaker allow to adjust the TEXMFHOME variable, you have to adjust the dedicated system-variable.

In Unix, you thus have to run texstudio/-maker from a terminal, e.g., within this repo:

```bash
export TEXMFHOME=$(pwd)/texmf; texstudio
```

and proceed as usual.

#### terminal

> **NOTE: only tested on Unix-systems** 

you can change the TEXMF-variable directly for the current terminal session

```bash
export TEXMFHOME=../texmf/
```

then proceed with your commands from the terminal, e.g.

```bash
pdflatex ...
xelatex ...
texstudio ...
```

### Custom Packages Overview

The following custom LaTeX-packages are available for this repo:

A custom all-in-one package is provided as [vgCustomDiss](./texmf/tex/vg/vgCustomDiss.sty),
which wraps the available custom packages as needed, depending on the provided options. 
This packages relies on the [tumDiss](./texmf/tex/tumDiss.cls) document class file.

More specifically, the individual custom-packages are given as:

- [vgBasics](./texmf/tex/vg/vgBasics.sty) contains the core custom macros and content that I have been constantly using.
- [vgBoxes](./texmf/tex/vg/vgBoxes.sty) contains some custom wrappers for the [tcolorbox](https://www.ctan.org/pkg/tcolorbox)-package.
- [vgCode](./texmf/tex/vg/vgCode.sty) contains some default configurations for pseudo-code, mainly relying on the [algorithm2e](https://www.ctan.org/pkg/algorithm2e)-package.
- [vgColors](./texmf/tex/vg/vgColors.sty) contains my default color palette with black-and-white or reduced color options.
- [vgFrames](./texmf/tex/vg/vgFrames.sty) contains [beamer](https://www.ctan.org/pkg/beamer)-specific modifications.
- [vgGloss](./texmf/tex/vg/vgGloss.sty) contains glossary package definitions (see below for included sub-packages)
- [vgTikz](./texmf/tex/vg/vgTikz.sty) wraps all [tikz](https://www.ctan.org/pkg/pgf) styles and packages I have been (re-)using.
- [vgTodo](./texmf/tex/vg/vgTodo.sty) adds some helper function around the [todonotes](https://www.ctan.org/pkg/todonotes)-package.

Furthermore, there are selective packages, which require the usage of the
[glossaries](https://www.overleaf.com/learn/latex/Glossaries) or the [vgGloss](./texmf/tex/vg/vgGloss.sty) package

- [vgAcros](./texmf/tex/vg/vgAcros.sty) contains the acronyms I have been (re-)using.
- [vgIndices](./texmf/tex/vg/vgIndices.sty) contains the indices I have been (re-)using.
- [vgNotation](./texmf/tex/vg/vgNotation.sty) contains the notation syntax of my custom macros.
- [vgOperators](./texmf/tex/vg/vgNotation.sty) contains the function and operator handles I have been (re-)using.
- [vgSymbols](./texmf/tex/vg/vgSymbols.sty) contains the symbols I have been (re-)using..

### Example usages

The available files should at best be self-explanatory, but in short:

- [article_example](./examples/article_example.tex) contains an example article and small tutorial based on a default LaTeX article.
- [beamer_example](./examples/beamer_example.tex) contains an example beamer presentation
- [thesis_example](./examples/thesis_example.tex) contains an example using the TUM-dissertation template
