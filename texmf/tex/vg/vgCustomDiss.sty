%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Volker Gabler Dissertation style file
% available options
%
% - notikzpics: disables tikz for \TikzInsertFig and \TikzInsert.
%   Note: this expects a PDF to exist in the graphics-path
% - nocode: do not include vgCode with desired settings, e.g. for standalone tikz
% - addstudentheses: add student thesis as seperate reference section
% - draft: enables draft-mode:
%   - enables colored hypterlinks for glossaries
%   - adds todo-package
%   - adds list of todos at end of thesis
% - print: enable reduced color-setting for printing
% - final: enables final-mode:
%   - set glossaries to final version
%   - enforce tikz and code-package usage
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\ProvidesPackage{vgCustomDiss}
\RequirePackage{ifthen}
\RequirePackage{xkeyval}
\RequirePackage[scr=kp]{mathalpha}
\RequirePackage{morewrites}
\RequirePackage{subcaption}
\RequirePackage{booktabs}
\RequirePackage{multirow}
\RequirePackage{longtable}
\RequirePackage{wrapfig}
\RequirePackage{tabulary}
\RequirePackage{amsthm}
\RequirePackage{floatrow} % figure and table side by side
\RequirePackage{lipsum}
\RequirePackage{fontawesome5} % icons
\RequirePackage{etoolbox} % toggle
%%% Flags
% globals
    
% package flags
\newif{\if@add@studWork}   \@add@studWorkfalse  % add student work as separate entry
\newif{\if@add@ownPubs}    \@add@ownPubstrue  % add student work as separate entry
\newif{\if@code}   	       \@codetrue  % import vgCde
\newif{\if@tikz}   	       \@tikztrue  % use tikz in TikzInsert
\newif{\if@version@draft}  \@version@draftfalse  % use draft mode
\newif{\if@version@print}  \@version@printfalse  % use print color, i.e., nocolors in vgColors
\newif{\if@version@final}  \@version@finalfalse  % set final settings -> add thx note etc
\DeclareOptionX{addstudentheses}{\@add@studWorktrue}
\DeclareOptionX{notikzpics}{\@tikzfalse}  % use PDFs instead of tikz-pictures
\DeclareOptionX{marl}{\MARLpapertrue}  % MARL->paper / chapter 6
\DeclareOptionX{print}{\@version@printtrue} % reduced color settings
\DeclareOptionX{nocode}{\@codefalse}  % disable pseudo-code packages
\DeclareOptionX{draft}{\@version@drafttrue\@version@finalfalse} % enable draft mode
\DeclareOptionX{final}{%  enable final mode
    \@version@finaltrue%
    \@version@draftfalse%
    \@add@studWorktrue%
    \@add@ownPubstrue%
    \@codetrue%
    \@tikztrue%
}



% add list of references and index to toc
\ifdefined\KOMAoptions
    \KOMAoptions{
            toc=bibliography,
            toc=index
    }
\fi
\ProcessOptionsX*

% option dependent package import
\if@version@print
    \RequirePackage[nocolors]{vgColors}
    \ifdefined\KOMAoptions
        \RequirePackage[bw]{vgBoxes}
    \fi
\else
    \ifdefined\KOMAoptions
        \RequirePackage{vgBoxes}
    \fi
    \RequirePackage{vgColors}
\fi
\RequirePackage{vgBasics}
\if@code
	\RequirePackage{vgCode}
\else 
    \newtoggle{addAlgos}
    \togglefalse{addAlgos}
\fi

\if@version@draft
    \RequirePackage[debug]{vgToDo}
\else
    \RequirePackage[slim]{vgToDo}
\fi


%%%%%%%%%%%%%%%%%%%%%%%%
% bib settings
% citing compatibility
\RequirePackage{bibunits}

% credits to: https://tex.stackexchange.com/questions/163451/total-number-of-citations
\ifdefined\fcite
    \renewcommand{\fcite}[2]{#1~#2}
\else
	\newcommand{\fcite}[2]{#1~#2}
\fi
\usepackage[authoryear,sort,comma]{natbib}
\def\VGbibstyle{ecta}
\renewcommand{\fcite}[2]{#2}
\if@version@draft
    \colorlet{vgHyperlink}{vg_dblue1}
\fi

\providecommand\phantomsection{}

\theoremstyle{remark}


\def\DissChapter/{chapter}

\newif\ifdoublecolumn
\doublecolumnfalse

% Function / commands
\newcommand{\resetAcros}{
    \ifdefined\acresetall
		\acresetall
	\fi
	\ifdefined\glsresetall
		\glsresetall
	\fi
}

\NewDocumentCommand{\TikzInsert}{O{} m m}{
	\if@tikz
		\centertikz[#1]{#2}[#3]
	\else
		\includegraphics[width=#3]{#2.pdf}
	\fi
}
\NewDocumentCommand{\TikzInsertFig}{O{} m m}{
	\if@tikz
		\centertikzfig[#1]{#2}[#3]
	\else
		\includegraphics[width=#3]{#2.pdf}
	\fi
}


\RequirePackage{vgTikz}

\usetikzlibrary{external}
\newcommand{\tikzpath}{tikz}





% chapter 9 plot colors
\colorlet{GPCRCol}{vgPlotCol1}
\colorlet{PIBUCol}{vgPlotCol2}
\colorlet{FGraphCol}{vgPlotCol3}
\colorlet{NBayesGraphCol}{vgPlotCol4}


\RequirePackage[
    bookmarksopen = true,
    bookmarksnumbered = true,
    plainpages = false,
    colorlinks = true,
    linkcolor = vgHyperlink,
    citecolor = vgHypercite,
    filecolor = vgHyperfile,
    urlcolor  = vgHyperurl
]{hyperref}
\usepackage[noabbrev,capitalize,nameinlink]{cleveref}

\usepackage[toc=true, section, acronym, shortcuts, style=alttree, nomain, nonumberlist]{glossaries}
\RequirePackage{vgGloss}

%% Dissertation specific acros
\newacronym[first=\emph{efficient policy}~(\acs{eff})]{eff}{E}{efficient policy in~\cref{sec:hgmdp:exp}}
\newacronym[first=\emph{\ac{HGMDP}-based policy}~(\acs{legFull})]{legFull}{L}{\ac{HGMDP}-policy in~\cref{sec:hgmdp:exp}}
\newacronym[first=\emph{partially \ac{HGMDP}-based policy with feedback}~(\acs{legPart})]{legPart}{LF}{partially \ac{HGMDP}-policy with feedback in~\cref{sec:hgmdp:exp}}
\newacronym[first=hybrid-force-velocity strategy~(\acs{hfv})]{hfv}{hybrid-f-v}{hybrid-force-velocity strategy in~\cref{sec:Grasp:experiment}}
\newacronym[first=hybrid-velocity-force strategy~(\acs{hvf})]{hvf}{hybrid-v-f}{hybrid-velocity-force strategy in~\cref{sec:Grasp:experiment}}
\newacronym[first=force-based strategy~(\acs{frcstrat})]{frcstrat}{frc-based}{force-based strategy in~\cref{sec:Grasp:experiment}}
\newacronym[first=velocity-based strategy~(\acs{velstrat}) ]{velstrat}{vel-based}{velocity-based strategy in~\cref{sec:Grasp:experiment}}
\newacronym[first=fixed behavior policy~(\acs{fixed}),sort=fixed]{fixed}{\emph{Fixed}}{fixed behavior policy in~\cref{sec:hrc_game:Experiment}}
\newacronym[first=spline game-policy~(\acs{spline}), sort=spline]{spline}{\emph{Spline}}{spline based human-prediction game-policy in~\cref{sec:hrc_game:Experiment}}
\newacronym[first=line game-policy~(\acs{line}), sort=line]{line}{\emph{Line}}{spline based human-prediction game-policy in~\cref{sec:hrc_game:Experiment}}

% IEEE compatibility
\newcommand{\PARstart}[2]{#1#2}
\let\labelindent\relax
% for IEEE and similar:
% https://tex.stackexchange.com/questions/170772/command-labelindent-already-defined

\if@version@print
    \graphicspath{{pics/misc_bw/}
                {pics/gt_icra/}
                {pics/hgmdp/}
                {pics/muagrl/}
                {pics/haptic_explorer/}
                {pics/bocBot/}
                {pics/grasper/}}
\else
    \graphicspath{{pics/misc/}
                {pics/gt_icra/}
                {pics/hgmdp/}
                {pics/muagrl/}
                {pics/haptic_explorer/}
                {pics/bocBot/}
                {pics/grasper/}}
\fi


\NewDocumentCommand{\vgAddCustomBib}{m m O{\VGbibstyle}}{
    \begingroup
        \let\cleardoublepage\relax
        \begin{bibunit}[#3]
            \renewcommand{\bibname}{#1}
            \nocite{*}                   % include all items in given bib file
            \putbib[#2]
        \end{bibunit}
    \endgroup
}

\newcommand{\vgAddBibs}[1][normal]{
    % publications
    \if@add@ownPubs
        \vgAddCustomBib{Own Thesis-Related Publications}{references/own_pubs}
    \fi
    % theses
    \if@add@studWork
        \begingroup
            \let\cleardoublepage\relax
            \begin{bibunit}[\VGbibstyle]
                \KOMAoptions{toc=nobib}
                \renewcommand{\bibname}{Supervised Student Theses}
                \nocite{*}                   % include all items in given bib file
                \putbib[references/students]
            \end{bibunit}
        \endgroup
    \fi
}


% add number of citation string
\newcommand{\vgPrintNumberOfCitations}{
    \ifdefined\vgNbCitation
        \begingroup
            \hypersetup{hidelinks}
            \footnotesize
            \begin{flushright}
            \emph{
                    \href{https://tinyurl.com/3n2kwchm}{This bibliography contains}
                    {\vgNbCitation}
                    {\color{black} \href{https://tinyurl.com/mwn77fz3}{references}.}
                    }
            \end{flushright}
        \endgroup
    \fi
}

\newcommand{\vgAddTodo}{
    \if@version@draft
        \phantomsection{}
        \listoftodos
        \addcontentsline{toc}{chapter}{List of Todos}
    \fi
}

\newcommand{\vgAlgorithmLists}{
    \begingroup
    \let\cleardoublepage\relax  % prevent page break for figures, tables, etc
        \phantomsection{}
        \addcontentsline{toc}{chapter}{Lists of Algorithms, Figures and Tables}
        \listofalgorithms
        \listoffigures
        \listoftables
    \endgroup
}

\newcommand{\vgLists}{
    \begingroup
    \let\cleardoublepage\relax  % prevent page break for figures, tables, etc
        \phantomsection{}
        \addcontentsline{toc}{chapter}{Lists of Figures and Tables}
        \listoffigures
        \listoftables
    \endgroup
}

\newcommand{\vgListOfStuff}{
    \iftoggle{addAlgos}{\vgAlgorithmLists}{\vgLists}
}

\newcommand{\vgDissGloss}{
    \phantomsection
    \chapter*{Glossary}\label{ch:glossary}
    \addcontentsline{toc}{chapter}{Glossary}
    \glssetwidest{DEC-POMDP}
    \printglossary[type=acronym]
    \glssetwidest{ABCD}
    \printglossary[type=notation, nogroupskip]
    \glssetwidest{ABC}
    \section*{List of Symbols}\label{sec:gloss:symbols}
    \addcontentsline{toc}{section}{List of Symbols}
    \setglossarysection{subsection}
    \printglossary[type=symbols:ap, nogroupskip]
    \printglossary[type=symbols:control, nogroupskip]
    \printglossary[type=symbols:graph, nogroupskip]
    \printglossary[type=symbols:mg, nogroupskip]
    \printglossary[type=symbols:ml, nogroupskip]
    \printglossary[type=symbols:num, nogroupskip]
    \glssetwidest{ABCDE}
    \printglossary[type=symbols:rob,   nogroupskip]
    \glssetwidest{ABC}
    \printglossary[type=symbols:space, nogroupskip]
    \printglossary[type=symbols:se3,   nogroupskip]
    \printglossary[type=symbols:hrc,   nogroupskip, title=Variables for~Part~\ref*{part:HRC}]
    \printglossary[type=symbols:muhrl, nogroupskip, title=Variables for~Part~\ref*{part:MuAg}]
    \printglossary[type=symbols:hrr,   nogroupskip, title=Variables for~Part~\ref*{part:RobEnv}]
    \setglossarysection{section}
    \glssetwidest{ABCD}
    \printglossary[type=indices, nogroupskip]
    \printglossary[type=operators, nogroupskip]
    \cleardoublepage
}

\makeglossaries
\hyphenation{par-am-eter-ized}
\hyphenation{ver-fahr-en}
\hyphenation{Ver-ur-teil-ung}
\hyphenation{La-bor-um-ge-bung-en}
\hyphenation{Mo-dell-wis-sen}
\hyphenation{In-dus-trie-ro-bo-ter}
\hyphenation{Ma-te-ri-al-ei-gen-schaft-en}
\hyphenation{For-schungs-fra-gen}

% Revision content
\NewDocumentCommand{\revised}{O{} O{} m}{#3}

\endinput
