\ProvidesClass{tumDiss}
\NeedsTeXFormat{LaTeX2e}

% --- Class options
% title page font style
\newif\if@titlepage@sansserif
\DeclareOption{seriftitlepage}{\@titlepage@sansseriffalse}%
\DeclareOption{sansseriftitlepage}{\@titlepage@sansseriftrue}%

% global option
\newif\ifblackwhiteprint
\DeclareOption{nocolors}{\blackwhiteprinttrue}

\newif\if@tikzlogos
\DeclareOption{tikzlogos}{\@tikzlogostrue}

\newif\if@disable@hypersetup
\DeclareOption{disablehypersetup}{\@disable@hypersetuptrue}

% title page layout lines
\newif\if@titlepage@showlayout
\DeclareOption{showlayout}{\@titlepage@showlayouttrue}%
\DeclareOption{noshowlayout}{\@titlepage@showlayoutfalse}%
% pass remaining options to scrbook class
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrbook}}%
% default options
\ExecuteOptions{seriftitlepage,noshowlayout,tikzlogos}%
\ProcessOptions\relax




% --- Derive from class scrbook
\LoadClass[%
  a4paper,
  twoside,
  parskip=half,
%  fontsize      = 11pt, % default: 11pt
%  chapterprefix = false,           % don't print "Chapter XX" before chapter headings
%  toc           = listof,          % include list of tables and figures in toc
%  toc           = bib,             % include bib in toc
  numbers       = noenddot,         % If you include an Appendix, each number will get
%                                   % an ugly dot at the end, e.g. "Figure 3.9.: XXX".
%                                   % This option prevents this and makes it
%                                   % "Figure 3.9: XXX" as it should be
  titlepage=firstiscover
]{scrbook}%

% type setting area options are not forwarded correctly if loaded as class options in \LoadClass!!
\KOMAoptions{
  	headinclude=true, % include header in textarea
	footinclude=false, % include footer in textarea
	BCOR=15mm,  % correction for binding
	DIV=15,	% overwritten by geometry package
	titlepage=firstiscover
}
\recalctypearea

% set up page
%\usepackage[bindingoffset=2cm,inner=1cm, vmargin=4cm]{geometry}
\usepackage{scrlayer-scrpage}
\usepackage{scrhack}
%\typearea[10mm]{12}

\setcapindent{1em} % overwrite default "hanging" caption paragraphs

% --- Load packages
% use manual text positioning on title page
\if@titlepage@showlayout
  \RequirePackage[showboxes,absolute]{textpos}%
  \usepackage{showframe} % shows typing area and margins
\else
  \RequirePackage[absolute]{textpos}%
\fi

% used for toc tweaking
\RequirePackage{etoolbox}%


% load tumcolor package (xcolor, pgfplots and a tum-plotcycle list)
\RequirePackage[pgfplots,svgnames,x11names]{tumcolor}%

% --- Document styles
% caption style
\addtokomafont{captionlabel}{\bfseries}%
\addtokomafont{caption}{\small}%

% define section style
\setkomafont{section}{\bfseries \Large}

% define subsection style
\setkomafont{subsection}{\bfseries \large}


\usepackage[palatino]{quotchap}
\setkomafont{disposition}{\bfseries} % colour of chapter name
\setkomafont{dictum}{\sffamily\slshape}
\renewcommand\quotefont{\sffamily\slshape} % colour of chapter quote
\addtokomafont{partentry}{\scshape}

\NewDocumentCommand{\chapterquote}{O{70mm} m m}{
	\begin{savequote}[#1]
			``\ignorespaces#2''
			\qauthor{--- #3}
	\end{savequote}
}


% --- Document properties
% parameters as set by the main file.
\newcommand{\faculty}[1]{\def\@faculty{#1}}%
\newcommand{\facultyLogo}[1]{\def\@facultyLogo{#1}}%
\newcommand{\degree}[1]{\def\@degree{#1}}%
\newcommand{\vorsitz}[1]{\def\@vorsitz{#1}}%
\newcommand{\erstpruef}[1]{\def\@erstpruef{#1}}%
\newcommand{\datesubmitted}[1]{\def\@datesubmitted{#1}}%



\newif\ifzweitpruefaffil
\newcommand{\zweitpruef}[2][]{%
  \def\@zweitpruef{#2}
  \ifx#1\empty\else
    \def\@zweitpruefaffil{#1}
    \zweitpruefaffiltrue
  \fi
}

\newif\ifdrittpruefset
\newif\ifdrittpruefaffil
\newcommand{\drittpruef}[2][]{%
  \ifx#2\empty\else
    \drittpruefsettrue
    \def\@drittpruef{#2}
    \ifx#1\empty\else
      \def\@drittpruefaffil{#1}
      \drittpruefaffiltrue
    \fi
  \fi
}

\newcommand{\dateaccepted}[1]{\def\@dateaccepted{#1}}


\newtoggle{addEidesErkl}
\newtoggle{addTumAlumni}
\newtoggle{acceptLaw}
\newtoggle{noGuttenberg}
\newtoggle{uniqueDiss}

\def\@citysubmitted{Munich}
\newcommand{\GenerateEidesErkl}[1][]{%
	% used for eidesstattliche Erklärung (check box)
	\RequirePackage{amssymb}%
	\RequirePackage{enumitem}
	\newlist{EidErklList}{itemize}{2}
	\def\checkLabel{\Large$\boxtimes $}
	\def\unCheckLabel{\Large$\square $}
	\setlist[EidErklList]{label=\checkLabel}
	
	\toggletrue{addEidesErkl}
	\toggletrue{addTumAlumni}
	\toggletrue{acceptLaw}
	\toggletrue{uniqueDiss}
	\toggletrue{noGuttenberg}

	\def\@EidErkl{#1}
}
\newcommand{\citysubmitted}[1]{\def\@citysubmitted{#1}}%


% --- Titlepage
% The title page is fully centered.
\def\titlepagebindingcor#1{\def\@titlepagebindingcor{#1}}
\def\@titlepagebindingcor{0mm}
\RequirePackage{calc}%

\def\TUMLogoWidth{19mm}

% Left and right margins are exactly the same.
\def\defaultwidth{150mm}%
\def\defaulthpos{30mm + \@titlepagebindingcor}%

% Vertical positions of title page entities
\def\titlevpos{100.2mm - 1mm}%
\def\informationvpos{147mm + 1.5mm}%
\def\examinervpos{196.5mm + 1.5mm}%
\def\datevpos{241mm + 2mm}%

% --- Robustify
% clear textpos box
% textpos interacts badly with some packages due to a conflict with \shipout
% https://tex.stackexchange.com/questions/66804/unexpected-output-using-tikz-textpos
\def\cleartextposbox{\global\setbox\TP@holdbox\vbox{}}

\AtBeginDocument{%
% pdf links: only if hyperref was loaded in main file
\@ifpackageloaded{hyperref}{%
  \hypersetup{%
		bookmarks    = true,         % show bookmarks bar?
		pdftoolbar   = true,         % show Acrobat’s toolbar?
		pdfmenubar   = true,         % show Acrobat’s menu?
		pdffitwindow = false,        % window fit to page when opened
		pdfstartview = {FitH},       % fits the width of the page to the window
		pdftitle     = {\@title},    % title
		pdfauthor    = {\@author},   % author
		pdfsubject   = {PhD Thesis}, % subject of the document
		pdfcreator   = {\@author},   % creator of the document
		pdfnewwindow = true,         % links in new window
		% colorlinks   = false		 % user colored text instead of frames for hyperrefs
	}}{}%

	\ifblackwhiteprint
		\if@disable@hypersetup
			\hypersetup{
				colorlinks=false,
				linkcolor=black,
				citecolor=black,
				filecolor=black,
				urlcolor=black
			}
		\fi
	\else
		\colorlet{chaptergrey}{TUMBlue} % colour of chapter number
		\addtokomafont{disposition}{\color{TUMBlue}} % colour of chapter name
		\addtokomafont{dictum}{\color{TUMBlue}}
		\renewcommand\quotefont{\sffamily\slshape\color{TUMBlue}} % colour of chapter quote
		\addtokomafont{partentry}{\textcolor{TUMBlueDarker}}
	\fi
}


\if@tikzlogos
 	%%%%%%%%%%%%%%%%%%%%%%
 	%%% Logo definitions
 	%%%%%%%%%%%%%%%%%%%%%%%
	\ifblackwhiteprint
		\colorlet{TikzLogoColor}{black}
	\else
		\colorlet{TikzLogoColor}{TUMBlue}
	\fi
	\RequirePackage{tikz}
	\@ifpackagelater{tikz}{2013/12/13}{%
		% Package is new enough
	}{%
		\PackageError{tikz}{Package tikz is too old, please install at least version 3.0.0.}%
		\endinput
	}
	\usetikzlibrary{calc}
	\tikzset{
		tumlogo/.pic={
			\fill[pic actions]
			(-0.95,  0.5 ) --
			(-0.22,  0.5 ) --
			(-0.22, -0.32) --
			(-0.02, -0.32) --
			(-0.02,  0.5 ) --
			( 0.95,  0.5 ) --
			( 0.95, -0.5 ) --
			( 0.75, -0.5 ) --
			( 0.75,  0.32) --
			( 0.55,  0.32) --
			( 0.55, -0.5 ) --
			( 0.37, -0.5 ) --
			( 0.37,  0.32) --
			( 0.16,  0.32) --
			( 0.16, -0.5 ) --
			(-0.4 , -0.5 ) --
			(-0.4 ,  0.32) --
			(-0.6 ,  0.32) --
			(-0.6 , -0.5 ) --
			(-0.79, -0.5 ) --
			(-0.79,  0.32) --
			(-0.95,  0.32) -- cycle;
		}
	}
	\tikzset{
		lsrlogo/.pic={
				\fill[TikzLogoColor]
				(-0.42,  0.37) --
				(-0.17,  0.25) --
				(-0.17,  0.32) --
				( 0.32,  0.32) --
				( 0.32, -0.07) --
				(-0.17, -0.07) --
				(-0.17, -0.17) --
				( 0.42, -0.17) --
				( 0.42,  0.42) --
				(-0.17,  0.42) --
				(-0.17,  0.5 ) --   cycle;
			\fill[lsr_red]
				( 0.42, -0.37) --
				( 0.17, -0.5 ) --
				( 0.17, -0.42) --
				(-0.42, -0.42) --
				(-0.42,  0.17) --
				( 0.17,  0.17) --
				( 0.17,  0.07) --
				(-0.32,  0.07) --
				(-0.32, -0.32) --
				( 0.17, -0.32) --
				( 0.17, -0.25) --   cycle;
		}
	}
	\tikzset{
		itrlogo/.pic={
		%\begin{scope}[cm={{1.66944,0.0,0.0,1.41938,(-20.65341,-29.76687)}}]% g2985
		\begin{scope}[cm={{.166944,0.0,0.0,-.141938,(-2.3,4.6)}}]% g2985
			% path5

			\path[fill=itr_blue] (1.7147,38.0926) -- (1.7147,25.9804) -- (4.2352,25.9804) --
			(4.2352,38.0926) -- (1.7147,38.0926) -- cycle;

			% path7
			\path[fill=itr_blue] (8.7700,38.0926) -- (8.7700,27.6418) -- (4.4325,27.6418) --
			(4.4325,25.9804) -- (15.6362,25.9804) -- (15.6362,27.6418) --
			(11.2987,27.6418) -- (11.2987,38.0926) -- (8.7700,38.0926) -- cycle;

			% path9
			\path[fill=itr_blue] (15.8300,38.0926) -- (15.8300,25.9804) -- (20.3640,25.9804)
			.. controls (22.8248,25.9804) and (24.0547,26.9817) .. (24.0547,28.9839) ..
			controls (24.0547,29.7313) and (23.8463,30.4120) .. (23.4286,31.0257) ..
			controls (23.0113,31.6394) and (22.4371,32.1100) .. (21.7061,32.4375) --
			(25.6672,38.0926) -- (22.5983,38.0926) -- (19.5947,33.1414) --
			(18.2036,33.1414) -- (18.2036,38.0926) -- (15.8300,38.0926) --
			cycle(18.2036,31.4800) -- (18.7846,31.4800) .. controls (20.6124,31.4800) and
			(21.5260,30.7408) .. (21.5260,29.2620) .. controls (21.5260,28.1816) and
			(20.7104,27.6414) .. (19.0790,27.6414) -- (18.2032,27.6414) --
			(18.2032,31.4800) -- cycle;
			\end{scope}
		}
  }
\fi


\newcommand\makeTUMtitle{

	\ifblackwhiteprint
		\colorlet{TitleFontColor}{black}
	\else
		\colorlet{TitleFontColor}{TUMBlue}
	\fi

	\if@titlepage@sansserif
		\sffamily
	\fi

	% Layouting helper
	\if@titlepage@showlayout
		% Logo box with 105% size rel. to TUM logo
		\begin{textblock*}{10.5mm}(9.75mm, 9.75mm)
			\hspace{1mm} \vspace{10.5mm}
		\end{textblock*}

		% top and bottom margin check for logo and faculty text
		\begin{textblock*}{180mm}(25mm, 10mm)
			\hspace{1mm} \vspace{10mm}
		\end{textblock*}

		\begin{textblock*}{190mm}(10mm, 148.5mm) % mid
			\noindent\makebox[\hsize]{\rule{\textwidth}{1pt}}
		\end{textblock*}

		\begin{textblock*}{190mm}(10mm, 99mm) % first third
			\noindent\makebox[\hsize]{\rule{\textwidth}{1pt}}
		\end{textblock*}

		\begin{textblock*}{190mm}(10mm, 198mm) % second third
			\noindent\makebox[\hsize]{\rule{\textwidth}{1pt}}
		\end{textblock*}

	\fi


	% Logos
	% Faculty logo
	\ifdefined\@facultyLogo
		\makeatletter
		\noindent
		\begin{textblock*}{10.5mm}(9.75mm + \@titlepagebindingcor, 9.75mm)
			\@facultyLogo
		\end{textblock*}
		\makeatother
	\fi

	% Faculty text
	%                         See TUM CD Handbook page 16
	%                         x    + x    + 0.5x
	\begin{textblock*}{120mm}(10mm + 10mm + 5mm + \@titlepagebindingcor, 13mm)
		\footnotesize
		\fontfamily{phv}\selectfont
		\noindent \textcolor{TitleFontColor}{
			{\@faculty}
		}\\
		\fontfamily{phv}\selectfont
		\textcolor{TitleFontColor}{
			Technische Universit\"at M\"unchen
		}
	\end{textblock*}

	% TUM logo
	\begin{textblock*}{\TUMLogoWidth}(\paperwidth - \TUMLogoWidth - 10mm, 10mm)
		\if@tikzlogos
			\begin{tikzpicture}
				\pic [TikzLogoColor](tumlogo) {tumlogo};
  			\end{tikzpicture}
		\else
			\noindent \includegraphics[height = 10mm]{TUM_Logo_blau_cmyk.pdf}
		\fi
	\end{textblock*}

	% Titel und Autor
	% Keep the name position fixed. Longer titles expand towards top.
	\begin{textblock*}{\defaultwidth}[0,1](\defaulthpos, \titlevpos)
		\centering

		{\bfseries \Large \textcolor{TitleFontColor}\@title}
		\ifdefempty{\@subtitle}{}{\vspace{1ex}\bfseries\large \textcolor{TitleFontColor} \@subtitle}

		\vspace{10mm}

		{\bfseries \large \@author}\\

	\end{textblock*}


	% Informationstext
	\begin{textblock*}{\defaultwidth}(\defaulthpos, \informationvpos)
		\centering
		\raggedright

		Vollst\"andiger Abdruck der von der {\@faculty} der Technischen
		Universit\"at M\"unchen zur Erlangung des akademischen Grades eines\\

		\vspace{6mm}

		\centering {\bfseries \large {\@degree}}\\

		\vspace{6mm}
		\raggedright

		genehmigten Dissertation.\\

	\end{textblock*}



	% Prüfer
	\begin{textblock*}{\defaultwidth}(\defaulthpos, \examinervpos)
		\centering
		\raggedright

		\textbf{Vorsitz:}\\
		\hspace{8mm}\hphantom{1. }\@vorsitz

		\vspace{2mm}

		\textbf{Pr\"ufer*innen der Dissertation:}\\
		\hspace{8mm}1. \@erstpruef\\
		\hspace{8mm}2. \@zweitpruef
		\ifzweitpruefaffil
		,\\\hspace{8mm}\hphantom{2. }\@zweitpruefaffil
		\fi
		\ifdrittpruefset
		\\\hspace{8mm}3. \@drittpruef
		\ifdrittpruefaffil
		,\\\hspace{8mm}\hphantom{2. }\@drittpruefaffil
		\fi
		\fi

	\end{textblock*}



	% Daten
	\begin{textblock*}{\defaultwidth}(\defaulthpos, \datevpos)
		\raggedright
		Die Dissertation wurde am {\@datesubmitted} bei der Technischen Universit\"at
		M\"unchen eingereicht%
		\ifdefined\@dateaccepted%
		~und durch die {\@faculty} am {\@dateaccepted} angenommen%
		\fi
		.
	\end{textblock*}
	%\end{titlepage}

	% force empty page
	%\mbox{}
	%\newpage
	%\thispagestyle{empty}
	%\null\newpage\cleartextposbox
	%\makeatother
} % end \renewcommand\maketitle

\renewcommand\maketitle[1][1]{ % based on scrextend.sty
    \begin{titlepage}
	\setcounter{page}{%
		#1%
	}%
	\if@titlepageiscoverpage
		\edef\titlepage@restore{%
			\noexpand\endgroup
			\noexpand\global\noexpand\@colht\the\@colht
			\noexpand\global\noexpand\@colroom\the\@colroom
			\noexpand\global\vsize\the\vsize
			\noexpand\global\noexpand\@titlepageiscoverpagefalse
			\noexpand\let\noexpand\titlepage@restore\noexpand\relax
		}%
		\begingroup
		\topmargin=\dimexpr \coverpagetopmargin-1in\relax
		\oddsidemargin=\dimexpr \coverpageleftmargin-1in\relax
		\evensidemargin=\dimexpr \coverpageleftmargin-1in\relax
		\textwidth=\dimexpr
		\paperwidth-\coverpageleftmargin-\coverpagerightmargin\relax
		\textheight=\dimexpr
		\paperheight-\coverpagetopmargin-\coverpagebottommargin\relax
		\headheight=0pt
		\headsep=0pt
		\footskip=\baselineskip
		\@colht=\textheight
		\@colroom=\textheight
		\vsize=\textheight
		\columnwidth=\textwidth
		\hsize=\columnwidth
		\linewidth=\hsize
	\else
		\let\titlepage@restore\relax
	\fi
	\let\footnotesize\small
	\let\footnoterule\relax
	\let\footnote\thanks
	\renewcommand*\thefootnote{\@fnsymbol\c@footnote}%
	\let\@oldmakefnmark\@makefnmark
	\renewcommand*{\@makefnmark}{\rlap\@oldmakefnmark}%
	\ifx\@extratitle\@empty
		\ifx\@frontispiece\@empty
		\else
			\if@twoside\mbox{}\next@tpage\fi
			\noindent\@frontispiece\next@tdpage
		\fi
	\else
		\noindent\@extratitle
		\ifx\@frontispiece\@empty
		\else
			\next@tpage
			\noindent\@frontispiece
		\fi
		\next@tdpage
	\fi
	\parskip\z@ \parindent\z@ \parfillskip\z@\@plus 1fil
	\ifx\@titlehead\@empty \else
		\begin{minipage}[t]{\textwidth}%
			\usekomafont{titlehead}{\@titlehead\par}%
		\end{minipage}\par
	\fi
	\null\vfill
%	\begin{center}
%		\ifx\@subject\@empty \else
%			{\usekomafont{subject}{\@subject\par}}%
%			\vskip 3em
%		\fi
%		{\usekomafont{title}{\huge \@title\par}}%
%		\vskip 1em
%		{\ifx\@subtitle\@empty\else\usekomafont{subtitle}{\@subtitle\par}\fi}%
%		\vskip 2em
%		{%
%			\usekomafont{author}{%
%				\lineskip 0.75em
%				\begin{tabular}[t]{c}
%					\@author
%				\end{tabular}\par
%			}%
%		}%
%		\vskip 1.5em
%		{\usekomafont{date}{\@date \par}}%
%		\vskip \z@ \@plus3fill
%		{\usekomafont{publishers}{\@publishers \par}}%
%		\vskip 3em
%	\end{center}\par

	\makeTUMtitle

	\iftoggle{addEidesErkl}{
			% TUM logo

			\chapter*{}
			\vspace*{1cm}
			\thispagestyle{empty}
			\section*{Eidesstattliche Erklärung}
			Ich, \@author, erkl\"are an Eides statt, dass ich die bei der 
			\@faculty~der 
			Technische Universit\"at M\"unchen 
			zur Promotionsprüfung vorgelegte Arbeit mit dem Titel:
			
			\begin{center}
				{\bfseries \textcolor{TitleFontColor}\@title}
			\end{center}

			unter der Anleitung und Betreuung durch: 
			\@erstpruef~ohne sonstige Hilfe erstellt und 
			bei der Abfassung nur die gemäß § 7 Abs. 6 und 7 
			angegebenen Hilfsmittel benutzt habe.
			
			\begin{EidErklList}
				\iftoggle{noGuttenberg}{\item}{\item[\unCheckLabel]}
					Ich habe keine Organisation eingeschaltet, die gegen Entgelt 
					Betreuer*innen für die Anfertigung von Dissertationen sucht, 
					oder die mir obliegenden Pflichten hinsichtlich der
					Prüfungsleistungen für mich ganz oder teilweise erledigt.
				\iftoggle{uniqueDiss}{\item}{\item[\unCheckLabel]}
					Ich habe die Dissertation in dieser oder ähnlicher 
					Form in keinem anderen Prüfungsverfahren
					als Prüfungsleistung vorgelegt.
				\iftoggle{acceptLaw}{\item}{\item[\unCheckLabel]}
					Ich habe keine Kenntnis über ein strafrechtliches 
					Ermittlungsverfahren in Bezug auf wissenschaftsbezogene 
					Straftaten gegen mich oder eine rechtskräftige strafrechtliche 
					Verurteilung mit Wissenschaftsbezug.
				{\@EidErkl}
			\end{EidErklList}

			Die öffentlich zugängliche Promotionsordnung sowie die 
			Richtlinien zur Sicherung guter wissenschaftlicher Praxis 
			und für den Umgang mit wissenschaftlichem Fehlverhalten 
			der TUM sind mir bekannt, insbesondere habe ich die 
			Bedeutung von § 27 PromO (Nichtigkeit der Promotion)
			und § 28 PromO (Entzug des Doktorgrades) zur Kenntnis genommen. 
			Ich bin mir der Konsequenzen einer falschen 
			Eidesstattlichen Erklärung bewusst.
			
			Mit der Aufnahme meiner personenbezogenen Daten 
			in die Alumni-Datei bei der TUM bin ich

			\begin{EidErklList}
				\iftoggle{addTumAlumni}{
					\item einverstanden
					\item[\unCheckLabel] nicht einverstanden
				}{
					\item[\unCheckLabel] einverstanden
					\item nicht einverstanden
				}
			\end{EidErklList}

			\vspace{3cm}


			\@citysubmitted, \@datesubmitted,  \@author
			% add TUM logo again
			\begin{textblock*}{\TUMLogoWidth}(\paperwidth - \TUMLogoWidth - 10mm, 10mm)
				\begin{tikzpicture}
					\pic [TikzLogoColor](tumlogo) {tumlogo};
				\end{tikzpicture}
			\end{textblock*}
	}{} % end eidesstattliche Erklärung (if)

	\@thanks\global\let\@thanks\@empty
	\vfill\null
	\if@twoside
		\@tempswatrue
		\expandafter\ifnum \@nameuse{scr@v@3.12}>\scr@compatibility\relax
		\else
			\ifx\@uppertitleback\@empty\ifx\@lowertitleback\@empty
				\@tempswafalse
			\fi\fi
		\fi
		\if@tempswa
			\next@tpage
			\begin{minipage}[t]{\textwidth}
				\@uppertitleback
			\end{minipage}\par
			\vfill
			\begin{minipage}[b]{\textwidth}
				\@lowertitleback
			\end{minipage}\par
			\@thanks\global\let\@thanks\@empty
		\fi
	\else
		\ifx\@uppertitleback\@empty\else
		\PackageWarning{scrextend}{%
			non empty \string\uppertitleback\space ignored
			by \string\maketitle\MessageBreak
			in `twoside=false' mode%
		}%
		\fi
		\ifx\@lowertitleback\@empty\else
			\PackageWarning{scrextend}{%
				non empty \string\lowertitleback\space ignored
				by \string\maketitle\MessageBreak
				in `twoside=false' mode%
			}%
		\fi
	\fi
	\ifx\@dedication\@empty
	\else
		\next@tdpage\null\vfill
		{\centering\usekomafont{dedication}{\@dedication \par}}%
		\vskip \z@ \@plus3fill
		\@thanks\global\let\@thanks\@empty
		\cleardoubleemptypage
	\fi
	\ifx\titlepage@restore\relax\else\clearpage\titlepage@restore\fi
\end{titlepage}
}

% --- toc tweaking
% make subsubsection numbered
\setcounter{secnumdepth}{3}

% --- make subsubsection appear in TOC
\setcounter{tocdepth}{2}

% Redefine Toc entry for \part command
\RedeclareSectionCommand[tocnumwidth=5em]{part}
\let\originaladdparttocentry\addparttocentry
\renewcommand*{\addparttocentry}[2]{%
		\IfArgIsEmpty{#1}{% entry without number
			\originaladdparttocentry{#1}{#2}%
		}{% entry with number
			\originaladdparttocentry{\partname~#1}{#2}%
		}%
}


% \ChapterOutsidePart and \ChapterInsidePart: control the bookmark level of each sectioning command
% source: https://latex.org/forum/viewtopic.php?p=11584&sid=4aa023af61f9144f792dd169d37294ed#p11584
\makeatletter
\newcommand{\ChapterOutsidePart}{%
	\addtocontents{toc}{\protect\vskip1cm}\hbox{} % vspacing in table of contents; reason for hbox cf. https://tex.stackexchange.com/questions/10291/addtocontents-at-end-of-document-not-getting-written-to-toc-file/10297#10297
	\def\toclevel@chapter{-1}\def\toclevel@section{0}\def\toclevel@subsection{1}
}
\newcommand{\ChapterInsidePart}{%
	\def\toclevel@chapter{0}\def\toclevel@section{1}\def\toclevel@subsection{2}
}
\makeatother

% G.Huber - Chapter with Footnote
\NewDocumentCommand{\ChapterWithFootnote}{o m m o}{
	\chapter[\IfNoValueTF{#1}{#2}{#1}] % for table of content (equal to header)
	{#2\\
		\normalfont{\footnotesize #3}}	% full title
		\IfArgIsEmpty{#4}{%empty label
		}{\label{#4}}
	\chaptermark{\IfNoValueTF{#1}{#2}{#1}} % for header cf. https://www.texfaq.org/FAQ-runheadtoobig
	% cf. https://tex.stackexchange.com/questions/367479/ifthenelse-fails-if-is-part-of-argument
}


\endinput
%% End of file `tumDiss.cls'.


